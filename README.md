# **CODENAME: *Venture***

[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

CODENAME: Venture is a fully open source DIY portable gaming system. Everything
from the printed circuit board (PCB) to the operating system (OS) is done in
house with some minor exceptions (e.g. The actual printing of the PCB, and
possibly the placement of components).

Venture is meant as a fun project and as a teaching and learn tool for embedded
design and programming. It is *not* meant as an actual competing system on the
market. Therefore, it will not have the processing power comparable to the
state of the art systems such as the PS5 or XBOX S. However, the system will be
fully open source and free<sup>1</sup>.

## Specification **(TEMP)**

### Functions
- Wifi - LAN/WAN Connectivity
- BLE - Peripherals (Controllers, Headphones, Speaker)
- Touchscreen - Main Display
- HDMI - External Display
- ROM - Bootloader
- Extrnal RAM - Volatile Memory
- Flash/SSD - Persistent Storage
- SDIO Card - External Persistent Storage
- Li-ion Battery - Portable Power
- 3.5mm Port - External Speaker/Headphones
- Speakers - Portable Audio
- GPU - Graphics Acceleration

### Network Stacks
1. WiFi
2. Ethernet
3. IP
4. TCP, UDP
5. SDP
6. ... (SSL, ...)<sup>2</sup>
7. ... (TELNET, HTTP, DNS, ...)<sup>2</sup>

### APIs
#### Hardware Enhanced
- MD5
- SHA
- AES

#### Provided
- RSA
- CRC
- SSL

#### OS Gated
- HID (USB)
- SDP (TCP/UDP)
- 'Window'
- 'Audio'
- malloc

## Repo Structure


##### Footnotes

1. While the code and tools may be free the physical products will have to be
made. ICs, components, PCBs, and the printing thereof cost money. Do not embark
on this project you don't have some discretionary spending.
2. The Presentation and Application layers vary by program and are typically
closely tied. While Venture will implement some of these, these layers will
largely by provided by the app developer.
